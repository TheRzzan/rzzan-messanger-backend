const portHttp = 80;
const portHttps = 443;

const express = require('express');
const http = require('http');
const https = require('https');
const fs = require('fs');
const cors = require('cors');
const cluster = require('cluster');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');

const router = express.Router();

module.exports = router;

const expressApp = express();
expressApp.listData = [];

const sqlWrapper = require('./node/db/sql_wrapper');
const chatDBHelper = require('./node/db/chat_db_helper')(sqlWrapper);
const accessTokenDBHelper = require('./node/db/auth/access_token_db_helper')(sqlWrapper);
const oAuthModel = require('./node/db/auth/access_token_model')(chatDBHelper, accessTokenDBHelper);
const oAuth2Server = require('node-oauth2-server');

expressApp.oauth = oAuth2Server({
    model: oAuthModel,
    grants: ['password'],
    debug: false
});

const chatHelper = require('./node/route/chat')(chatDBHelper, express.Router(), express.Router())
const authRoutes = require('./node/route/auth/auth_routs')(express.Router(), expressApp, chatDBHelper);

expressApp.use(bodyParser.urlencoded({extended: true}));
expressApp.use(bodyParser.json());
expressApp.use(logger('dev'));
expressApp.use(cookieParser());
expressApp.use(cors());
expressApp.use(helmet());
expressApp.use(expressApp.oauth.errorHandler());

expressApp.use('/sendMessage', expressApp.oauth.authorise(), chatHelper.router1)
expressApp.use('/getMessages', expressApp.oauth.authorise(), chatHelper.router2)
expressApp.use('/auth', authRoutes);

expressApp.use(function (err, req, res, next) {
    console.error('ERROR: ', err);
    res.status(500);
    if (err !== null) {
        if (err.name === 'OAuth2Error') {
            if (err.code !== null) {
                res.status(err.code);
            } else {
                res.status(404);
            }
        }
    }
    res.send((err === null) ? 'Something broke!' : err);
});

const workers = process.env.WORKERS || require('os').cpus().length;
// init the server
if (cluster.isMaster) {
    console.log('start cluster with %s workers', workers);

    for (var i = 0; i < workers; ++i) {
        const worker = cluster.fork().process;
        console.log('worker %s started.', worker.pid);
    }

    cluster.on('exit', function (worker) {
        console.log('worker %s stop.', worker.process.pid);
        cluster.fork();
    });
} else {
    http.createServer(expressApp)
        .listen(portHttp, function () {
            console.log('listening http on port ' + portHttp + '! Go to http://' + 'node' + ':' + portHttp);
        });

    https.createServer({
        key: fs.readFileSync('ssl/server.key'),
        cert: fs.readFileSync('ssl/server.cert')
    }, expressApp)
        .listen(portHttps, function () {
            console.log('listening https on port ' + portHttps + '! Go to https://' + 'node' + ':' + portHttps);
        });
}