let pgConnection

module.exports = injectedPgConnection => {
    pgConnection = injectedPgConnection

    create_db_structure()

    return {
        insert_new_message: insert_new_message,
        get_messages_list: get_messages_list,
        insert_new_token: insert_new_token,
        get_tokens_list: get_tokens_list,
        get_user_from_crentials: get_user_from_crentials,
        register_user_in_DB: register_user_in_DB,
        doesUserExist: doesUserExist,
    }
}

const sjcl = require('sjcl')

function create_db_structure() {
    pgConnection.query(
        'create table IF NOT exists chat ' +
        '( ' +
        '    id         serial not null primary key, ' +
        '    user_name  text   not null, ' +
        '    message    text   not null' +
        '); ', [], (error, results) => {
            // nothing
        })
    pgConnection.query(
        'create table IF NOT exists push_registration_tokens ' +
        '( ' +
        '    token     text not null primary key' +
        '); ', [], (error, results) => {
            // nothing
        })
    pgConnection.query(
        'create table IF NOT exists users ' +
        '( ' +
        '    id            serial not null primary key, ' +
        '    login         text   not null, ' +
        '    user_password text   not null, ' +
        '    unique (login) ' +
        '); ', [], (error, results) => {
            // nothing
        })
    pgConnection.query(
        'create table IF NOT exists oauth_access_tokens ' +
        '( ' +
        '    user_id      int                         not null primary key REFERENCES users (id), ' +
        '    access_token text                        not null, ' +
        '    expires      timestamp without time zone not null ' +
        '); ', [], (error, results) => {
            // nothing
        })
}

function doesUserExist(username, callback) {

    //create query to check if the user already exists
    const doesUserExistQuery = `SELECT *
                              FROM users
                              WHERE login = $1`

    //holds the results  from the query
    const sqlCallback = (error, results) => {

        //calculate if user exists or assign null if results is null
        const doesUserExist = results !== null && results.rows !== null ? results.rows.length > 0 : null

        //check if there are any users with this username and return the appropriate value
        callback(error, doesUserExist)
    }

    //execute the query to check if the user exists
    pgConnection.query(doesUserExistQuery, [username], sqlCallback)
}

function get_user_from_crentials (username, password, callback) {
    const shaPassword = sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(password))

    //create query using the data in the req.body to register the user in the db
    const query = `SELECT *
                 FROM users
                 WHERE (login = $1 AND user_password = $2)`

    //execute the query to get the user
    pgConnection.query(query, [username, shaPassword], (error, results) => {

        //pass in the error which may be null and pass the results object which we get the user from if it is not null
        callback(false, results !== null && results.rows !== null && results.rows.length === 1 ? results.rows[0] : null)
    })
}

function register_user_in_DB (username, password, registrationCallback) {

    //create query using the data in the req.body to register the user in the db
    const shaPassword = sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(password))
    console.log(shaPassword)
    const registerUserQuery = `INSERT INTO users (login, user_password)
                             VALUES ($1, $2)
                             on conflict (login) DO UPDATE SET user_password = $2
                             RETURNING *`

    //execute the query to register the user
    pgConnection.query(registerUserQuery, [username, shaPassword], registrationCallback)
}

function insert_new_message(data, callback) {
    const query = `INSERT
                 INTO chat (user_name, message)
                 VALUES ($1, $2)
                 RETURNING *;`
    pgConnection.query(query, data, (error, results) => {
        callback(error, results)
    })
}

function get_messages_list(callback) {
    const query = `SELECT *
                 FROM chat`
    pgConnection.query(query, [], (error, results) => {
        callback(error, results)
    })
}

function insert_new_token(data, callback) {
    const query = `INSERT
                 INTO push_registration_tokens (token)
                 VALUES ($1)
                 ON CONFLICT DO NOTHING
                 RETURNING *;`
    pgConnection.query(query, data, (error, results) => {
        callback(error, results)
    })
}

function get_tokens_list(except, callback) {
    const query = `SELECT *
                 FROM push_registration_tokens
                 WHERE (token != $1)`
    pgConnection.query(query, except, (error, results) => {
        callback(error, results)
    })
}
