let pgConnection;
module.exports = injectedMySqlConnection => {

    pgConnection = injectedMySqlConnection;

    return {
        saveAccessToken: saveAccessToken,
        getUserIDFromBearerToken: getUserIDFromBearerToken,
    };
};

function saveAccessToken(accessToken, userID, expires, callback) {
    const getUserQuery = `INSERT INTO oauth_access_tokens (user_id, access_token, expires)
                          VALUES ($1, $2, $3)
                          ON CONFLICT (user_id) DO UPDATE SET access_token = $2,
                                                              expires      = $3;`;

    //execute the query to get the user
    pgConnection.query(getUserQuery, [userID, accessToken, expires], (error) => {

        //pass in the error which may be null and pass the results object which we get the user from if it is not null
        callback(error);
    });
}

function getUserIDFromBearerToken(bearerToken, callback) {

    //create query to get the userID from the row which has the bearerToken
    const getUserIDQuery = `SELECT *
                            FROM oauth_access_tokens
                            WHERE access_token = $1;`;

    pgConnection.query(getUserIDQuery, [bearerToken], (error, results) => {
        //get the userID from the results if its available else assign null
        const userID = results !== null && results.rows.length === 1 ?
            results.rows[0] : null;
        callback(userID);
    });
}
