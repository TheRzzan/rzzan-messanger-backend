let userDBHelper;
let accessTokensDBHelper;

module.exports = (injectedUserDBHelper, injectedAccessTokensDBHelper) => {

    userDBHelper = injectedUserDBHelper;

    accessTokensDBHelper = injectedAccessTokensDBHelper;

    return {

        getClient: getClient,

        saveAccessToken: saveAccessToken,

        getUser: getUser,

        grantTypeAllowed: grantTypeAllowed,

        getAccessToken: getAccessToken,

    };
};

function getClient(clientID, clientSecret, callback) {

    const client = {
        clientID,
        clientSecret,
        grants: null,
        redirectUris: null
    };

    callback(false, client);
}

/* Determines whether or not the client which has to the specified clientID is permitted to use the specified grantType.
  The callback takes an eror of type truthy, and a boolean which indcates whether the client that has the specified clientID
  is permitted to use the specified grantType. As we're going to hardcode the response no error can occur
  hence we return false for the error and as there is there are no clientIDs to check we can just return true to indicate
  the client has permission to use the grantType. */
function grantTypeAllowed(clientID, grantType, callback) {

    console.log('grantTypeAllowed called and clientID is: ', clientID, ' and grantType is: ', grantType);

    callback(false, true);
}

function getUser(username, password, callback) {
    //try and get the user using the user's credentials
    userDBHelper.get_user_from_crentials(username, password, callback);
}

/* saves the accessToken along with the userID retrieved the specified user */
function saveAccessToken(accessToken, clientID, expires, user, callback) {
    //save the accessToken along with the user.id
    accessTokensDBHelper.saveAccessToken(accessToken, user.id, expires, callback);
}

function getAccessToken(accessToken, callback) {

    //try and get the userID from the db using the bearerToken
    accessTokensDBHelper.getUserIDFromBearerToken(accessToken, (data) => {

        //create the token using the retrieved userID
        const accessToken = {
            user: {
                user_id: data === null ? null : data.user_id,
            },
            expires: data === null ? null : data.expires
        };

        //set the error to true if userID is null, and pass in the token if there is a userID else pass null
        callback(false, data == null ? null : accessToken);
    });
}
