let userDBHelper

module.exports = (router, expressApp, injectedUserDBHelper) => {
    userDBHelper = injectedUserDBHelper;

    //route for registering new users
    router.post('/register', registerUser);

    //route for allowing existing users to login
    router.post('/login', expressApp.oauth.grant(), login);

    return router;
};

function registerUser(req, res) {
    //query db to see if the user exists already
    userDBHelper.doesUserExist(req.body.username, (sqlError, doesUserExist) => {

        //check if the user exists
        if (sqlError !== null || doesUserExist) {

            //message to give summary to client
            const message = sqlError !== null ? sqlError : 'User already exists';

            sendResponse(res, message, sqlError);

            return;
        }
        //register the user in the db
        userDBHelper.register_user_in_DB(req.body.username, req.body.password, (error) => {
            //create message for the api response
            const message = error === null ? 'Registration was successful' : 'Failed to register user';

            sendResponse(res, message, error);
        });
    });
}

function login(registerUserQuery, res) {
    sendResponse(res, 'test', null);
}

function sendResponse(res, message, error) {

    res
        .status(error === null ? 200 : 400)
        .json({
            'message': message,
            'error': error,
        });
}
