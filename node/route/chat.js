let chatDB

module.exports = (injectedChatDB, router1, router2) => {
    chatDB = injectedChatDB
    router1.post('/', send_message);
    router2.get('/', get_messages_list);
    return {
        router1: router1,
        router2: router2,
    }
};

const url = require('url');
const admin = require('firebase-admin');
const serviceAccount = require("./service-account-file.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://test-config-file-ad918-default-rtdb.firebaseio.com'
});

function send_message(req, res) {
    const queryObject = url.parse(req.url,true).query;

    let token = queryObject.token
    let user_name = queryObject.user_name
    let message = queryObject.message

    if (token === undefined) {
        res.status(401).json({response: 'Unauthorized'});
        return
    } else {
        chatDB.insert_new_token([token], (_, __) => {})
    }

    if (user_name === undefined || message === undefined) {
        res.status(400).json({response: 'Bad request'});
        return
    }

    chatDB.insert_new_message([user_name, message], (_, __) => {})

    send_out_message(user_name, message, token)

    res.status(200).json({response: 'Success'});
}

function send_out_message(user, msg, from_token) {
    chatDB.get_tokens_list([from_token], (error, results) => {
        if (error === null && results !== null) {
            const user_list = []
            results.rows.forEach(value => {
                user_list.push(value.token)
            });
            if (user_list.length <= 0)
                return
            const message = {
                data: {user_name: user, message: msg},
                tokens: user_list.filter( function( el ) {
                    return el !== from_token
                }),
            }

            admin.messaging().sendMulticast(message)
                .then((response) => {
                    // Response is a message ID string.
                    console.log('Successfully sent message: ', response);
                })
        }
    })
}

function get_messages_list(req, res) {
    chatDB.get_messages_list((error, results) => {
        const messages = []
        if (error === null && results !== null) {
            results.rows.forEach(value => {
                messages.push({
                    user_name: value.user_name,
                    message: value.message,
                })
            });
        }
        res.status(200).json({data: messages});
    })
}
